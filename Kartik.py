import re
from random import randint, shuffle
from typing import List

green_color: str = "[0;32;40m"
red_color: str = "[0;31;40m"


def add_impl(x: int, y: int) -> None:
    while True:
        result = int(input(f'{x} + [] =  {x + y} '))
        if result == y:
            print(chr(27) + "[2J")
            print(f'{x} + \x1b{green_color}{y} \x1b[0m = {x + y} . is correct ')
            return
        else:
            print(chr(27) + "[2J")
            print(f'sorry your answer \x1b{red_color}{result}\x1b[0m is wrong.. Try again')


def add():
    while True:
        print(chr(27) + "[2J")
        a = randint(0, 100 if 1 == randint(1, 2) else 10)
        b = randint(0, 10)
        add_impl(a, b)
        still_continue = input('To continue, type y or n : ')
        if still_continue == 'n':
            return


def subtract_impl(x: int, y: int) -> None:
    while True:
        result = int(input(f'{x} - {y} =   '))
        if result == (x - y):
            print(chr(27) + "[2J")
            print(f'{x} - {y} = {result} . \x1b{green_color}Correct \x1b[0m')
            return
        else:
            print(chr(27) + "[2J")
            print(f'sorry your answer \x1b{red_color}{result}\x1b[0m is wrong.. Try again')


def subtract():
    while True:
        print(chr(27) + "[2J")
        a = randint(0, 100 if 1 == randint(1, 2) else 10)
        b = randint(0, 10)
        subtract_impl(a if a > b else b, b if b < a else a)
        still_continue = input('To continue, type y or n : ')
        if still_continue == 'n':
            return


def before_impl(before_num: int):
    while True:
        result = int(input(f'what number is before {before_num}: ? '))
        if result == before_num - 1:
            print(chr(27) + "[2J")
            print(f'[\x1b{green_color}{before_num -1}\x1b[0m,{before_num}]. Your answer is correct :-)')
            return
        else:
            print(chr(27) + "[2J")
            print(f'Sorry your answer [\x1b{red_color}{result}\x1b[0m,{before_num}] is wrong')


def before():
    while True:
        before_impl(randint(1, 30))
        still_continue = input('To continue, type y or n : ')
        if still_continue == 'n':
            return


def after_impl(after_num: int):
    while True:
        result = int(input(f'what number is after {after_num}: ? '))
        if result == after_num + 1:
            print(chr(27) + "[2J")
            print(f'[{after_num}, \x1b{green_color}{result}\x1b[0m]. Your answer is correct :-)')
            return
        else:
            print(chr(27) + "[2J")
            print(f'Sorry your answer [{after_num}, \x1b{red_color}{result}\x1b[0m] is wrong')


def after():
    while True:
        after_impl(randint(1, 30))
        still_continue = input('To continue, type y or n : ')
        if still_continue == 'n':
            return


def missing_number_impl(list_upto: int):
    missing_num = randint(2, list_upto - 2)
    list_of_nums = list(range(1, list_upto + 1))
    list_of_nums.remove(missing_num)

    stringify_list_of_nums = f'{list_of_nums}'
    correct_msg = f'Good. Your answer is correct. {list(range(1, list_upto + 1))}'
    wrong_msg = f'Sorry your answer is wrong {list_of_nums}'

    while True:
        result = int(input(f'Find the missing number between {list_of_nums} '))
        if result == missing_num:
            re_result = re.findall(r' ?' + f'{missing_num}', correct_msg)
            if len(re_result) > 0:
                replace_with = ' \x1b' + green_color + f'{missing_num}' + '\x1b[0m'
                space_or_empty = ' ' if re_result[0][0] == ' ' else ''
                stringify_list = re.sub(re_result[0], f'{space_or_empty}{replace_with}', correct_msg, 1)
                print(chr(27) + "[2J")
                print(stringify_list)
            return
        else:
            print(chr(27) + "[2J")
            re_result = re.findall(r'\b' + f'{result}' + r'{1}\b', wrong_msg)
            if len(re_result) > 0:
                replace_with = '\x1b' + red_color + f'{result}' + '\x1b[0m'
                space_or_empty = ' ' if re_result[0][0] == ' ' else ''
                stringify_list = re.sub(r'\b' + re_result[0] + r'{1}\b', f'{space_or_empty}{replace_with}',
                                        f'{wrong_msg}.\nThe number, {result}, you entered is already in the list.')
                print(chr(27) + "[2J")
                print(stringify_list)
            else:
                print(chr(27) + "[2J")
                print('Sorry your answer is outside of this list.')
                colorify_item = '\x1b' + red_color + f'{result}' + '\x1b[0m'
                print(f'{stringify_list_of_nums} {colorify_item}')


def missing_number():
    while True:
        missing_number_impl(randint(15, 20))
        still_continue = input('To continue, type y or n : ')
        if still_continue == 'n':
            return


def sort_items_impl(ulimit: int):
    l_limit = randint(1, ulimit - 2)
    list_of_nums = [i for i in range(l_limit, ulimit + 1)]
    shuffle(list_of_nums)
    sorted_list = [i for i in range(l_limit, ulimit + 1)]
    while True:
        count = 0
        answer_till_now: List[int] = []
        while count < len(list_of_nums):
            print(chr(27) + "[2J")  # clear screen
            print(list_of_nums)
            till_now = f'{answer_till_now}. Type a number and press Enter.'
            num = int(input(till_now))
            count += 1
            answer_till_now.append(num)
        if answer_till_now == sorted_list:
            print(f'Good. Your answer is correct. {answer_till_now}')
            return
        else:
            print(f'Sorry, your answer is wrong. {answer_till_now}')
            still_continue = input('Would you like to try again ? y or n : ')
            if still_continue.lower() == "n":
                return


def sort_items():
    while True:
        sort_items_impl(randint(5, 15))
        still_continue = input('Do you want to try another set ? y or n : ')
        if still_continue == 'n':
            return


FUNCTION_MAP = {"a": add, "s": subtract, "b": before, "af": after, "m": missing_number, "ar": sort_items}


def menu():
    while True:
        print("Hello Kartik!")
        print('Enter a for Addition')
        print('Enter s for subtraction')
        print('Enter b for Before')
        print('Enter af for After')
        print('Enter m for Missing number')
        print('Enter ar for arrange items in order')
        print('Enter q for Quit')
        x = input("Enter your choice: ")
        if x.strip().lower() == 'q':
            break
        else:
            FUNCTION_MAP[x]()


if __name__ == '__main__':
    menu()
